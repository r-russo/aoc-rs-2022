use std::collections::{HashSet, VecDeque};

#[derive(Debug)]
struct HeightMap {
    start: (i32, i32),
    end: (i32, i32),
    map: Vec<i32>,
    height: usize,
    width: usize,
}

impl HeightMap {
    pub fn get_height(&self, x: i32, y: i32) -> i32 {
        if x >= 0 && y >= 0 {
            match self.map.get(x as usize + (y as usize) * self.width) {
                Some(x) => *x,
                None => 99,
            }
        } else {
            99
        }
    }

    pub fn get_neighbors(&self, x: i32, y: i32) -> Vec<(i32, i32)> {
        let mut neighbors = Vec::new();
        let current_height = self.get_height(x, y);

        for (dx, dy) in [(1, 0), (0, 1), (-1, 0), (0, -1)] {
            let (nx, ny) = (x + dx, y + dy);
            let next_height = self.get_height(nx, ny);
            if next_height - current_height <= 1 {
                neighbors.push((nx, ny));
            }
        }
        neighbors
    }
}

fn parse(input: &str) -> HeightMap {
    let mut hm = HeightMap {
        start: (0, 0),
        end: (0, 0),
        map: Vec::new(),
        height: 0,
        width: 0,
    };

    for (i, line) in input.trim().split('\n').enumerate() {
        hm.width = if hm.width == 0 { line.len() } else { 0 };
        hm.height += 1;

        for (j, c) in line.chars().enumerate() {
            if c == 'S' {
                hm.start = (j as i32, i as i32);
                hm.map.push(0);
            } else if c == 'E' {
                hm.end = (j as i32, i as i32);
                hm.map.push('z' as i32 - 97 + 1);
            } else {
                hm.map.push(c as i32 - 97);
            }
        }
    }

    hm
}

fn bfs(hm: &HeightMap, new_start: Option<(i32, i32)>) -> i32 {
    let mut visited = HashSet::new();
    let mut queue = VecDeque::new();

    let start = match new_start {
        Some(x) => x,
        None => hm.start,
    };

    visited.insert(start);
    queue.push_back((start, 0));

    while !queue.is_empty() {
        let ((x, y), path_length) = queue.pop_front().unwrap();

        if (x, y) == hm.end {
            return path_length;
        }

        for (nx, ny) in hm.get_neighbors(x, y) {
            if !visited.contains(&(nx, ny)) {
                visited.insert((nx, ny));
                queue.push_back(((nx, ny), path_length + 1));
            }
        }
    }

    -1
}

pub fn part1(input: &str) -> String {
    let hm = parse(input);
    bfs(&hm, None).to_string()
}

pub fn part2(input: &str) -> String {
    let hm = parse(input);
    let start_points = hm
        .map
        .iter()
        .enumerate()
        .filter(|(_, &x)| x == 0)
        .map(|(ix, _)| ix as i32);

    let mut lengths = Vec::new();
    for ix in start_points {
        let start = (ix % (hm.width as i32), ix / (hm.width as i32));
        let length = bfs(&hm, Some(start));
        if length != -1 {
            lengths.push(length);
        }
    }

    lengths.iter().min().unwrap().to_string()
}
