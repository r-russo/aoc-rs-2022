fn extract_players(line: &str) -> (char, char) {
    let mut chrs = line.chars();
    let c1 = chrs
        .next()
        .expect("Malformed line. Can't find first character");
    let c2 = chrs
        .nth(1)
        .expect("Malformed line. Can't find third character");
    (c1, c2)
}

fn score(p1: char, p2: char) -> Option<i64> {
    let mut ix = match p1 {
        'A' => 0,
        'B' => 2,
        'C' => 1,
        _ => return None,
    };

    ix += match p2 {
        'X' => 0,
        'Y' => 1,
        'Z' => 2,
        _ => return None,
    };

    let mut score = match ix % 3 {
        0 => 3,
        1 => 6,
        2 => 0,
        _ => return None,
    };

    score += match p2 {
        'X' => 1,
        'Y' => 2,
        'Z' => 3,
        _ => return None,
    };

    Some(score)
}

fn reverse_play(p1: char, outcome: char) -> Option<char> {
    let mut ix = match p1 {
        'A' => 2,
        'B' => 0,
        'C' => 1,
        _ => return None,
    };

    ix += match outcome {
        'X' => 0,
        'Y' => 1,
        'Z' => 2,
        _ => return None,
    };

    let code: u32 = <char as Into<u32>>::into('X') + ix % 3;

    Some(code.try_into().expect("Not a valid character"))
}

pub fn part1(input: &str) -> String {
    let mut total_score: i64 = 0;
    for line in input.trim().split('\n') {
        let (c1, c2) = extract_players(line);
        total_score += score(c1, c2).expect("Malformed line. Can't compute score");
    }
    total_score.to_string()
}

pub fn part2(input: &str) -> String {
    let mut total_score: i64 = 0;
    for line in input.trim().split('\n') {
        let (c1, c2) = extract_players(line);
        let p2 = reverse_play(c1, c2).expect("Malformed line");
        total_score += score(c1, p2).expect("Malformed line. Can't compute score");
    }
    total_score.to_string()
}
