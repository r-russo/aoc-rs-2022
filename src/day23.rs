use std::collections::{HashMap, HashSet};

fn parse(input: &str) -> HashSet<(i32, i32)> {
    let mut elfs = HashSet::new();

    for (i, line) in input.trim().split('\n').enumerate() {
        elfs.extend(
            line.chars()
                .enumerate()
                .filter(|(_, c)| *c == '#')
                .map(|(j, _)| (j.try_into().unwrap(), i.try_into().unwrap())),
        );
    }
    elfs
}

fn get_neighbours(position: &(i32, i32)) -> Vec<(i32, i32)> {
    vec![
        (position.0 - 1, position.1),
        (position.0 - 1, position.1 + 1),
        (position.0, position.1 + 1),
        (position.0 + 1, position.1 + 1),
        (position.0 + 1, position.1),
        (position.0 + 1, position.1 - 1),
        (position.0, position.1 - 1),
        (position.0 - 1, position.1 - 1),
    ]
}

fn first_half_of_round(
    elfs: &HashSet<(i32, i32)>,
    round: usize,
) -> HashMap<(i32, i32), (i32, i32)> {
    let mut proposed_moves = HashMap::new();
    let moves = vec![
        (
            (-1..=1).map(|x| (x, -1)).collect::<Vec<(i32, i32)>>(),
            (0, -1),
        ),
        (
            (-1..=1).map(|x| (x, 1)).collect::<Vec<(i32, i32)>>(),
            (0, 1),
        ),
        (
            (-1..=1).map(|y| (-1, y)).collect::<Vec<(i32, i32)>>(),
            (-1, 0),
        ),
        (
            (-1..=1).map(|y| (1, y)).collect::<Vec<(i32, i32)>>(),
            (1, 0),
        ),
    ];
    for &elf in elfs.iter() {
        if !get_neighbours(&elf).iter().any(|p| elfs.contains(p)) {
            proposed_moves.insert(elf, elf);
            continue;
        }

        for i in 0..moves.len() {
            let (current_move, proposed_move) = &moves[(i + round) % 4];
            if current_move
                .iter()
                .all(|p| elfs.get(&(elf.0 + p.0, elf.1 + p.1)).is_none())
            {
                proposed_moves.insert(elf, (elf.0 + proposed_move.0, elf.1 + proposed_move.1));
                break;
            }
        }
    }

    proposed_moves
}

fn simulate_round(mut elfs: HashSet<(i32, i32)>, round: i32) -> HashSet<(i32, i32)> {
    let proposed_moves = first_half_of_round(&elfs, round.try_into().unwrap());

    for (elf, next_position) in proposed_moves.iter() {
        if proposed_moves
            .iter()
            .any(|(other_elf, other_position)| other_elf != elf && other_position == next_position)
        {
            continue;
        }

        elfs.remove(elf);
        elfs.insert(*next_position);
    }

    elfs
}

fn count_empty_tiles(elfs: &HashSet<(i32, i32)>) -> i32 {
    let topleft = elfs
        .iter()
        .fold((9999, 9999), |(xp, yp), (x, y)| (*x.min(&xp), *y.min(&yp)));
    let bottomright = elfs
        .iter()
        .fold((0, 0), |(xp, yp), (x, y)| (*x.max(&xp), *y.max(&yp)));

    let total_tiles = (bottomright.0 - topleft.0 + 1) * (bottomright.1 - topleft.1 + 1);
    let total_elfs: i32 = elfs.len().try_into().unwrap();
    total_tiles - total_elfs
}

pub fn part1(input: &str) -> String {
    let mut elfs = parse(input);

    for round in 0..10 {
        let last_elfs = elfs;
        elfs = simulate_round(last_elfs.clone(), round);
        if last_elfs == elfs {
            break;
        }
    }

    count_empty_tiles(&elfs).to_string()
}

pub fn part2(input: &str) -> String {
    let mut elfs = parse(input);

    for round in 0.. {
        let last_elfs = elfs;
        elfs = simulate_round(last_elfs.clone(), round);
        if last_elfs == elfs {
            return (round + 1).to_string();
        }
    }

    (-1).to_string()
}
