use std::collections::VecDeque;

#[derive(Debug)]
struct Monkey {
    items: VecDeque<i64>,
    inspected_items: i64,
    operator: char,
    update_amount: i64,
    test_amount: i64,
    throw_true: usize,
    throw_false: usize,
}

fn parse_monkey(input: &str) -> Monkey {
    let mut items: Option<VecDeque<i64>> = None;
    let mut operator = ' ';
    let mut update_amount = 0;
    let mut test_amount = 0;
    let mut throw_true = 0;
    let mut throw_false = 0;

    for line in input.trim().split('\n') {
        let mut it = line.trim().split(':');
        let first = it.next().unwrap();

        if first.starts_with("Starting") {
            items = Some(
                it.next()
                    .unwrap()
                    .split(',')
                    .map(|x| x.trim().parse().unwrap())
                    .collect(),
            );
        } else if first.starts_with("Operation") {
            let mut rh = it
                .next()
                .unwrap()
                .split('=')
                .nth(1)
                .unwrap()
                .split_whitespace();

            operator = rh.nth(1).unwrap().chars().next().unwrap();
            let amount_string = rh.next().unwrap();
            if amount_string == "old" {
                operator = '^';
            } else {
                update_amount = amount_string.parse().unwrap();
            }
        } else if first.starts_with("Test") {
            test_amount = it
                .next()
                .unwrap()
                .split_whitespace()
                .last()
                .unwrap()
                .parse()
                .unwrap();
        } else if first.starts_with("If true") {
            throw_true = it
                .next()
                .unwrap()
                .split_whitespace()
                .last()
                .unwrap()
                .parse()
                .unwrap();
        } else if first.starts_with("If false") {
            throw_false = it
                .next()
                .unwrap()
                .split_whitespace()
                .last()
                .unwrap()
                .parse()
                .unwrap();
        }
    }

    Monkey {
        items: items.unwrap(),
        inspected_items: 0,
        operator,
        update_amount,
        test_amount,
        throw_true,
        throw_false,
    }
}

fn parse(input: &str) -> Vec<Monkey> {
    let mut monkeys = Vec::new();
    for monkey in input.trim().split("Monkey") {
        if monkey.is_empty() {
            continue;
        }
        monkeys.push(parse_monkey(monkey));
    }

    monkeys
}

fn get_congruent(value: i64, n: i64) -> i64 {
    value - value / n * n
}

fn simulate_single_round(monkeys: &mut Vec<Monkey>, base: i64, part2: bool) {
    for i in 0..monkeys.len() {
        while monkeys[i].items.is_empty() {
            let item = monkeys[i].items.pop_front().unwrap();
            let test_amount = monkeys[i].test_amount;
            let update_amount = monkeys[i].update_amount;
            monkeys[i].inspected_items += 1;
            let mut new_value = match monkeys[i].operator {
                '+' => item + update_amount,
                '*' => item * update_amount,
                '^' => item * item,
                _ => unimplemented!(),
            };
            if !part2 {
                new_value /= 3;
            }
            let throw = if new_value % test_amount == 0 {
                monkeys[i].throw_true
            } else {
                monkeys[i].throw_false
            };
            monkeys[throw]
                .items
                .push_back(get_congruent(new_value, base));
        }
    }
}

fn simulate_rounds(monkeys: &mut Vec<Monkey>, rounds: i64, base: i64, part2: bool) -> i64 {
    for _ in 0..rounds {
        simulate_single_round(monkeys, base, part2);
    }

    let mut inspected: Vec<i64> = Vec::new();
    for monkey in monkeys {
        inspected.push(monkey.inspected_items);
    }
    inspected.sort();
    let mut it = inspected.iter().rev();

    it.next().unwrap() * it.next().unwrap()
}

pub fn part1(input: &str) -> String {
    let mut monkeys = parse(input);
    let base = monkeys
        .iter()
        .map(|x| x.test_amount)
        .reduce(|x, y| x * y)
        .unwrap();
    simulate_rounds(&mut monkeys, 20, base, false).to_string()
}

pub fn part2(input: &str) -> String {
    let mut monkeys = parse(input);
    let base = monkeys
        .iter()
        .map(|x| x.test_amount)
        .reduce(|x, y| x * y)
        .unwrap();
    simulate_rounds(&mut monkeys, 10000, base, true).to_string()
}
