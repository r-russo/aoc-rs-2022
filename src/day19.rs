use std::collections::HashMap;

#[derive(Debug)]
struct Blueprint {
    ore_cost: usize,
    clay_cost: usize,
    obsidian_cost: (usize, usize),
    geode_cost: (usize, usize),
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
struct Materials {
    ore: usize,
    clay: usize,
    obsidian: usize,
    geode: usize,
}

impl Materials {
    pub fn new_robots() -> Materials {
        Materials {
            ore: 1,
            clay: 0,
            obsidian: 0,
            geode: 0,
        }
    }

    pub fn new_materials() -> Materials {
        Materials {
            ore: 0,
            clay: 0,
            obsidian: 0,
            geode: 0,
        }
    }
}

fn parse(input: &str) -> Vec<Blueprint> {
    let mut blueprints = Vec::new();
    for line in input.trim().split('\n') {
        let (_, specs) = line.split_once(": ").unwrap();
        let mut blueprint = Blueprint {
            ore_cost: 0,
            clay_cost: 0,
            obsidian_cost: (0, 0),
            geode_cost: (0, 0),
        };
        for (i, spec) in specs.split(". ").enumerate() {
            let mut it = spec.split_whitespace();
            match i {
                0 => blueprint.ore_cost = it.nth(4).unwrap().parse().unwrap(),
                1 => blueprint.clay_cost = it.nth(4).unwrap().parse().unwrap(),
                2 => {
                    blueprint.obsidian_cost = (
                        it.nth(4).unwrap().parse().unwrap(),
                        it.nth(2).unwrap().parse().unwrap(),
                    )
                }
                3 => {
                    blueprint.geode_cost = (
                        it.nth(4).unwrap().parse().unwrap(),
                        it.nth(2).unwrap().parse().unwrap(),
                    )
                }
                _ => panic!("Unexpected value"),
            };
        }
        blueprints.push(blueprint);
    }
    blueprints
}

fn collect(materials: &Materials, robots: &Materials) -> Materials {
    Materials {
        ore: materials.ore + robots.ore,
        clay: materials.clay + robots.clay,
        obsidian: materials.obsidian + robots.obsidian,
        geode: materials.geode + robots.geode,
    }
}

fn simulate(
    blueprint: &Blueprint,
    materials: &Materials,
    robots: &Materials,
    time_left: usize,
    cache: &mut HashMap<(Materials, Materials, usize), usize>,
    max_geode_count: &mut usize,
) -> usize {
    if time_left == 0 {
        *max_geode_count = materials.geode.max(*max_geode_count);
        return materials.geode;
    }

    if let Some(&c) = cache.get(&(materials.clone(), robots.clone(), time_left)) {
        return c;
    }

    let new_materials = collect(materials, robots);

    // if new_materials.geode < max_geode_count[time_left - 1] {
    //     return new_materials.geode;
    // } else {
    //     max_geode_count[time_left - 1] = new_materials.geode;
    // }

    let can_build = materials.geode + time_left * (robots.geode + robots.geode + time_left) / 2;

    if can_build < *max_geode_count {
        return materials.geode;
    }

    let mut geodes = 0;

    let can_build_ore_robot = materials.ore >= blueprint.ore_cost
        && robots.ore < blueprint.obsidian_cost.0.max(blueprint.geode_cost.1);
    let can_build_clay_robot =
        materials.ore >= blueprint.clay_cost && robots.clay < blueprint.obsidian_cost.1;
    let can_build_obsidian_robot = materials.ore >= blueprint.obsidian_cost.0
        && materials.clay >= blueprint.obsidian_cost.1
        && robots.obsidian < blueprint.geode_cost.1;
    let can_build_geode_robot =
        materials.ore >= blueprint.geode_cost.0 && materials.obsidian >= blueprint.geode_cost.1;

    let can_build_robot = can_build_obsidian_robot
        && can_build_ore_robot
        && can_build_clay_robot
        && can_build_geode_robot;

    if can_build_geode_robot {
        geodes = geodes.max(simulate(
            blueprint,
            &Materials {
                ore: new_materials.ore - blueprint.geode_cost.0,
                obsidian: new_materials.obsidian - blueprint.geode_cost.1,
                ..new_materials
            },
            &Materials {
                geode: robots.geode + 1,
                ..*robots
            },
            time_left - 1,
            cache,
            max_geode_count,
        ));
    } else {
        if can_build_obsidian_robot {
            geodes = geodes.max(simulate(
                blueprint,
                &Materials {
                    ore: new_materials.ore - blueprint.obsidian_cost.0,
                    clay: new_materials.clay - blueprint.obsidian_cost.1,
                    ..new_materials
                },
                &Materials {
                    obsidian: robots.obsidian + 1,
                    ..*robots
                },
                time_left - 1,
                cache,
                max_geode_count,
            ));
        }

        if can_build_clay_robot {
            geodes = geodes.max(simulate(
                blueprint,
                &Materials {
                    ore: new_materials.ore - blueprint.clay_cost,
                    ..new_materials
                },
                &Materials {
                    clay: robots.clay + 1,
                    ..*robots
                },
                time_left - 1,
                cache,
                max_geode_count,
            ));
        }

        if can_build_ore_robot {
            geodes = geodes.max(simulate(
                blueprint,
                &Materials {
                    ore: new_materials.ore - blueprint.ore_cost,
                    ..new_materials
                },
                &Materials {
                    ore: robots.ore + 1,
                    ..*robots
                },
                time_left - 1,
                cache,
                max_geode_count,
            ));
        }
    }

    if !can_build_robot {
        geodes = geodes.max(simulate(
            blueprint,
            &new_materials,
            robots,
            time_left - 1,
            cache,
            max_geode_count,
        ));
    }

    cache.insert((materials.clone(), robots.clone(), time_left), geodes);

    geodes
}

pub fn part1(input: &str) -> String {
    let mut sum_quality_levels = 0;
    let blueprints = parse(input);
    for (i, blueprint) in blueprints.iter().enumerate() {
        let mut cache = HashMap::new();
        sum_quality_levels += (i + 1)
            * simulate(
                blueprint,
                &Materials::new_materials(),
                &Materials::new_robots(),
                24,
                &mut cache,
                &mut 0,
            );
    }
    sum_quality_levels.to_string()
}

pub fn part2(input: &str) -> String {
    let mut product_of_max_geodes = 1;
    let blueprints = parse(input);
    for (i, blueprint) in blueprints.iter().enumerate() {
        if i == 3 {
            break;
        }
        let mut cache = HashMap::new();
        let max_geodes = simulate(
            blueprint,
            &Materials::new_materials(),
            &Materials::new_robots(),
            32,
            &mut cache,
            &mut 0,
        );
        product_of_max_geodes *= max_geodes;
    }
    product_of_max_geodes.to_string()
}
