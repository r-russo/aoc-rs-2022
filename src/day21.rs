use std::collections::HashMap;

fn parse(input: &str) -> HashMap<String, String> {
    let mut monkeys = HashMap::new();

    for line in input.trim().split('\n') {
        let (name, operation) = line.split_once(": ").unwrap();
        monkeys.insert(name.to_string(), operation.to_string());
    }

    monkeys
}

fn get_result(monkey: &str, monkeys: &HashMap<String, String>, compare_root: bool) -> Option<i64> {
    let operation = monkeys.get(monkey)?;

    match operation.parse() {
        Ok(n) => Some(n),
        Err(_) => {
            let mut parts = operation.split_whitespace();
            let monkey1 = parts.next().unwrap();
            let op = if compare_root && monkey == "root" {
                parts.next();
                "-"
            } else {
                parts.next().unwrap()
            };
            let monkey2 = parts.next().unwrap();
            let fun = match op {
                "+" => |a: i64, b: i64| a + b,
                "-" => |a, b| a - b,
                "*" => |a, b| a * b,
                "/" => |a, b| a / b,
                _ => panic!("Unrecognized operation `{}`", op),
            };
            Some(fun(
                get_result(monkey1, monkeys, compare_root)?,
                get_result(monkey2, monkeys, compare_root)?,
            ))
        }
    }
}

pub fn part1(input: &str) -> String {
    let monkeys = parse(input);
    get_result("root", &monkeys, false).unwrap().to_string()
}

pub fn part2(input: &str) -> String {
    let mut monkeys = parse(input);
    monkeys.remove("humn");
    for monkey in monkeys.clone().keys() {
        if let Some(r) = get_result(monkey, &monkeys, false) {
            monkeys.remove(monkey);
            monkeys.insert(monkey.to_string(), r.to_string());
        }
    }
    let mut min = -10_000_000_000_000i64;
    let mut max = 10_000_000_000_000i64;
    let mut humn = (min + max) / 2;
    while min < max {
        monkeys.remove("humn");
        let humn_str = humn.to_string();
        monkeys.insert("humn".to_string(), humn_str);
        let r = get_result("root", &monkeys, true).unwrap();

        if r == 0 {
            break;
        } else if r > 0 {
            min = humn;
        } else if r < 0 {
            max = humn;
        }

        humn = (min + max) / 2;
    }
    humn.to_string()
}
