struct PairSectionID {
    first: (i32, i32),
    second: (i32, i32),
}

fn parse_pairs(input: &str) -> Vec<PairSectionID> {
    let mut ret: Vec<PairSectionID> = Vec::new();
    for line in input.split('\n') {
        let mut pair = PairSectionID {
            first: (-1, -1),
            second: (-1, -1),
        };
        for (i, p) in line.split(',').enumerate() {
            let mut ids = p.split('-');
            let start: i32 = ids
                .next()
                .expect("Invalid format")
                .parse()
                .expect("Not a number");
            let end: i32 = ids
                .next()
                .expect("Invalid format")
                .parse()
                .expect("Not a number");
            if i == 0 {
                pair.first = (start, end);
            } else {
                pair.second = (start, end);
                break;
            }
        }
        ret.push(pair);
    }
    ret
}

fn full_overlap(pair: PairSectionID) -> bool {
    let (first, second) = (pair.first, pair.second);

    first.0 <= second.0 && first.1 >= second.1 || second.0 <= first.0 && second.1 >= first.1
}

fn overlap(pair: PairSectionID) -> bool {
    let (first, second) = (pair.first, pair.second);

    full_overlap(pair)
        || first.1 >= second.0 && first.0 <= second.0
        || second.1 >= first.0 && second.0 <= first.0
}

pub fn part1(input: &str) -> String {
    let pairs = parse_pairs(input.trim());
    let mut count = 0;
    for pair in pairs {
        if full_overlap(pair) {
            count += 1;
        }
    }
    count.to_string()
}

pub fn part2(input: &str) -> String {
    let pairs = parse_pairs(input.trim());
    let mut count = 0;
    for pair in pairs {
        if overlap(pair) {
            count += 1;
        }
    }
    count.to_string()
}
