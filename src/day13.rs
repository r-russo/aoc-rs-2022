use std::cmp::Ordering;

fn parse(input: &str) -> Vec<(&str, &str)> {
    let mut pairs = Vec::new();
    for pair in input.trim().split("\n\n") {
        pairs.push(pair.split_once('\n').unwrap());
    }

    pairs
}

fn get_nth_element(list: &str, n: i32) -> Option<&str> {
    let mut level = 0;
    let mut element = 0;

    let list_without_parens = if list.starts_with('[') {
        &list[1..list.len() - 1]
    } else {
        list
    };
    let mut start = 0;
    let mut end = list_without_parens.len();

    for (i, c) in list_without_parens.chars().enumerate() {
        match c {
            '[' => level += 1,
            ']' => {
                level -= 1;
            }
            ',' => {
                if level == 0 {
                    if element == n {
                        end = i;
                        break;
                    } else if element == n - 1 {
                        start = i + 1;
                    }
                    element += 1;
                }
            }
            _ => (),
        }
    }

    if n == element && start < end {
        Some(&list_without_parens[start..end])
    } else {
        None
    }
}

fn compare_pairs(left: &str, right: &str) -> Ordering {
    if let (Ok(l), Ok(r)) = (left.parse::<i32>(), right.parse::<i32>()) {
        let r = (r - l).signum();
        return match r.cmp(&0) {
            Ordering::Equal => Ordering::Equal,
            Ordering::Less => Ordering::Greater,
            Ordering::Greater => Ordering::Less,
        };
    };

    for i in 0.. {
        let element_left = get_nth_element(left, i);
        let element_right = get_nth_element(right, i);

        match (element_left, element_right) {
            (Some(l), Some(r)) => {
                let result = compare_pairs(l, r);
                match result {
                    Ordering::Equal => continue,
                    _ => return result,
                }
            }
            (None, Some(_)) => return Ordering::Less,
            (Some(_), None) => return Ordering::Greater,
            (None, None) => break,
        }
    }

    Ordering::Equal
}

pub fn part1(input: &str) -> String {
    let pairs = parse(input);
    let mut result = 0;
    for (i, pair) in pairs.iter().enumerate() {
        if compare_pairs(pair.0, pair.1) == Ordering::Less {
            result += i + 1;
        }
    }
    result.to_string()
}

pub fn part2(input: &str) -> String {
    let divider_packets = ("[[2]]", "[[6]]");
    let complete_input =
        input.replace("\n\n", "\n") + &format!("{}\n{}", divider_packets.0, divider_packets.1);
    let mut sorted_inputs: Vec<&str> = complete_input.split('\n').collect();
    sorted_inputs.sort_by(|a, b| compare_pairs(a, b));
    let ix1 = sorted_inputs
        .iter()
        .position(|x| x == &divider_packets.0)
        .unwrap();
    let ix2 = sorted_inputs[ix1..]
        .iter()
        .position(|x| x == &divider_packets.1)
        .unwrap()
        + ix1;
    ((ix1 + 1) * (ix2 + 1)).to_string()
}
