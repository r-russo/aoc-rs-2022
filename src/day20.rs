fn parse(input: &str) -> Vec<i64> {
    let mut file = Vec::new();
    for line in input.trim().split('\n') {
        file.push(line.parse().unwrap());
    }
    file
}

fn mix(file: Vec<i64>, repeat: i32) -> Vec<i64> {
    let n = file.len();
    let n_i64 = i64::try_from(n).unwrap();
    let mut indexes: Vec<_> = (0..n).collect();

    for _ in 0..repeat {
        for i in 0..n {
            let index = indexes.iter().position(|&x| x == i).unwrap();
            let start = index;
            let start_i64 = i64::try_from(start).unwrap();
            let item = file[indexes[index]];

            let delta = start_i64 + item;
            let end = if delta < 0 {
                delta.rem_euclid(n_i64 - 1)
            } else {
                delta % (n_i64 - 1)
            };
            let end_us = usize::try_from(end.abs()).unwrap();

            indexes.remove(index);
            indexes.insert(end_us, i);
        }
    }

    indexes.iter().map(|&x| file[x]).collect()
}

fn get_groove_coordinates(file: &Vec<i64>) -> i64 {
    let zero_pos = file.iter().position(|&x| x == 0).unwrap();
    file[(zero_pos + 1000) % file.len()]
        + file[(zero_pos + 2000) % file.len()]
        + file[(zero_pos + 3000) % file.len()]
}

pub fn part1(input: &str) -> String {
    let mut file = parse(input);
    file = mix(file, 1);
    get_groove_coordinates(&file).to_string()
}

pub fn part2(input: &str) -> String {
    let encrypted_file = parse(input);
    let decryption_key = 811589153;
    let mut file = encrypted_file.iter().map(|v| v * decryption_key).collect();
    file = mix(file, 10);
    get_groove_coordinates(&file).to_string()
}
