type StackType = Vec<Vec<char>>;
type InstructionType = (usize, usize, usize);

fn parse_input(input: &str) -> (StackType, Vec<InstructionType>) {
    let mut parsing_initial_state = true;
    let mut first = true;
    let mut n = 0;
    let mut tmp_stacks: Vec<Vec<char>> = Vec::new();
    let mut instructions: Vec<(usize, usize, usize)> = Vec::new();
    for line in input.split('\n') {
        let mut crates: Vec<char> = Vec::new();
        if line.is_empty() {
            parsing_initial_state = false;
            continue;
        }

        if parsing_initial_state {
            if first {
                first = false;
                n = line.len();
            }

            let line_chars: Vec<char> = line.chars().collect();
            crates.push(line_chars[1]);
            for i in 1..(n + 1) / 4 {
                crates.push(line_chars[1 + 4 * i]);
            }

            tmp_stacks.push(crates);
        } else {
            let words: Vec<&str> = line.split_whitespace().collect();
            if words.len() <= 5 {
                panic!("Malformed instruction");
            }
            let c: usize = words[1].parse().expect("Not a number");
            let from: usize = words[3].parse().expect("Not a number");
            let to: usize = words[5].parse().expect("Not a number");
            instructions.push((c, from, to));
        }
    }

    tmp_stacks.reverse();

    let mut stacks: Vec<Vec<char>> = Vec::new();
    for (i, row) in tmp_stacks.iter().enumerate() {
        for (j, col) in row.iter().enumerate() {
            if i == 0 || *col == ' ' {
                continue;
            } else if i == 1 {
                stacks.push(vec![*col]);
            } else {
                stacks[j].push(*col);
            }
        }
    }

    (stacks, instructions)
}

pub fn part1(input: &str) -> String {
    let (mut stacks, instructions) = parse_input(input);
    for instruction in instructions {
        let (n, from, to) = instruction;
        for _ in 0..n {
            let c = stacks[from - 1].pop();
            stacks[to - 1].push(c.expect("Not valid"));
        }
    }

    let mut r = String::new();

    for stack in stacks {
        r = format!("{}{}", r, stack.last().expect("Empty stack"));
    }
    r
}

pub fn part2(input: &str) -> String {
    let (mut stacks, instructions) = parse_input(input);
    for instruction in instructions {
        let (n, from, to) = instruction;
        let mut tmp: Vec<char> = Vec::new();
        for _ in 0..n {
            let c = stacks[from - 1].pop();
            tmp.push(c.expect("Empty array"));
        }
        tmp.reverse();
        stacks[to - 1].extend(tmp);
    }

    let mut r = String::new();

    for stack in stacks {
        r = format!("{}{}", r, stack.last().expect("Empty stack"));
    }
    r
}
