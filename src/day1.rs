fn get_food_count(input: &str) -> Vec<i32> {
    let mut elfs: Vec<i32> = Vec::new();

    let mut current_calories_count = 0;
    for food in input.split('\n') {
        if !food.is_empty() {
            current_calories_count += food.parse::<i32>().expect("Calories count is not a number");
        } else {
            elfs.push(current_calories_count);
            current_calories_count = 0;
        }
    }
    if current_calories_count != 0 {
        elfs.push(current_calories_count);
    }
    elfs
}

pub fn part1(input: &str) -> String {
    let elfs: Vec<i32> = get_food_count(input);
    (*elfs).iter().max().expect("Vector is empty").to_string()
}

pub fn part2(input: &str) -> String {
    let mut elfs = get_food_count(input);
    elfs.sort();
    (elfs[elfs.len() - 1] + elfs[elfs.len() - 2] + elfs[elfs.len() - 3]).to_string()
}
