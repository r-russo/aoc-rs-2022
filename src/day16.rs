use std::collections::HashMap;

#[derive(Debug, PartialEq, Eq)]
struct Valve {
    flow_rate: i32,
    tunnels: Vec<String>,
    index: u32,
}

fn parse(input: &str) -> HashMap<String, Valve> {
    let mut map = HashMap::new();

    for (index, line) in input.trim().split('\n').enumerate() {
        let (valve, tunnel_names) = line.split_once("; ").unwrap();

        let mut it_valve = valve.split_whitespace();
        let valve_name = it_valve.nth(1).unwrap();
        let flow_rate: i32 = it_valve
            .nth(2)
            .unwrap()
            .split_once('=')
            .unwrap()
            .1
            .parse()
            .unwrap();

        let mut tunnels = Vec::new();
        for tunnel in tunnel_names.split(", ") {
            tunnels.push(tunnel.split_whitespace().last().unwrap().to_string());
        }

        map.insert(
            valve_name.to_string(),
            Valve {
                flow_rate,
                tunnels,
                index: index as u32,
            },
        );
    }

    map
}

fn is_valve_closed(opened_valves: u64, valve: u32) -> bool {
    opened_valves & (1 << valve) == 0
}

fn open_valve(opened_valves: u64, valve: u32) -> u64 {
    opened_valves | (1 << valve)
}

fn find_max_pressure(
    map: &HashMap<String, Valve>,
    cache: &mut HashMap<(String, u64, i32), i32>,
    valve: &str,
    opened_valves: u64,
    flow_rate: i32,
    time_left: i32,
) -> i32 {
    if time_left == 0 {
        return 0;
    }

    if let Some(&pressure) = cache.get(&(valve.to_string(), opened_valves, time_left)) {
        return pressure;
    }

    let mut pressure = 0;

    let current_valve = map.get(valve).unwrap();
    if is_valve_closed(opened_valves, current_valve.index) && current_valve.flow_rate > 0 {
        pressure = find_max_pressure(
            map,
            cache,
            valve,
            open_valve(opened_valves, current_valve.index),
            flow_rate + current_valve.flow_rate,
            time_left - 1,
        );
    }

    for next in &current_valve.tunnels {
        let p = find_max_pressure(map, cache, next, opened_valves, flow_rate, time_left - 1);
        pressure = p.max(pressure);
    }

    pressure += flow_rate;
    cache.insert((valve.to_string(), opened_valves, time_left), pressure);

    pressure
}

fn find_max_pressure_with_elephant(
    map: &HashMap<String, Valve>,
    cache: &mut HashMap<(String, String, u64, i32), i32>,
    valve: &str,
    elephants_valve: &str,
    opened_valves: u64,
    flow_rate: i32,
    time_left: i32,
) -> i32 {
    if time_left == 0 {
        return 0;
    }

    if let Some(&pressure) = cache.get(&(
        valve.to_string(),
        elephants_valve.to_string(),
        opened_valves,
        time_left,
    )) {
        return pressure;
    }

    let mut pressure = 0;

    let current_valve = map.get(valve).unwrap();
    let current_elephants_valve = map.get(elephants_valve).unwrap();

    let closed = is_valve_closed(opened_valves, current_valve.index) && current_valve.flow_rate > 0;
    let closed_for_elephant = is_valve_closed(opened_valves, current_elephants_valve.index)
        && current_elephants_valve.flow_rate > 0
        && valve != elephants_valve;

    match (closed, closed_for_elephant) {
        (true, true) => {
            let new_opened = open_valve(opened_valves, current_elephants_valve.index)
                | open_valve(opened_valves, current_valve.index);
            let new_flow_rate =
                flow_rate + current_valve.flow_rate + current_elephants_valve.flow_rate;
            let p = find_max_pressure_with_elephant(
                map,
                cache,
                valve,
                elephants_valve,
                new_opened,
                new_flow_rate,
                time_left - 1,
            );
            pressure = pressure.max(p);
        }
        (true, false) => {
            let new_opened = open_valve(opened_valves, current_valve.index);
            let new_flow_rate = flow_rate + current_valve.flow_rate;

            for next_elephant in current_elephants_valve.tunnels.iter() {
                let p = find_max_pressure_with_elephant(
                    map,
                    cache,
                    valve,
                    next_elephant,
                    new_opened,
                    new_flow_rate,
                    time_left - 1,
                );
                pressure = pressure.max(p);
            }
        }
        (false, true) => {
            let new_opened = open_valve(opened_valves, current_elephants_valve.index);
            let new_flow_rate = flow_rate + current_elephants_valve.flow_rate;
            for next in current_valve.tunnels.iter() {
                let p = find_max_pressure_with_elephant(
                    map,
                    cache,
                    next,
                    elephants_valve,
                    new_opened,
                    new_flow_rate,
                    time_left - 1,
                );
                pressure = pressure.max(p);
            }
        }
        (false, false) => {
            for next in current_valve.tunnels.iter() {
                for next_elephant in current_elephants_valve.tunnels.iter() {
                    let p = find_max_pressure_with_elephant(
                        map,
                        cache,
                        next,
                        next_elephant,
                        opened_valves,
                        flow_rate,
                        time_left - 1,
                    );
                    pressure = p.max(pressure);
                }
            }
        }
    }

    pressure += flow_rate;
    cache.insert(
        (
            valve.to_string(),
            elephants_valve.to_string(),
            opened_valves,
            time_left,
        ),
        pressure,
    );

    pressure
}

pub fn part1(input: &str) -> String {
    let map = parse(input);

    let mut cache = HashMap::new();
    find_max_pressure(&map, &mut cache, "AA", 0, 0, 30).to_string()
}

pub fn part2(input: &str) -> String {
    let map = parse(input);

    let mut cache = HashMap::new();
    find_max_pressure_with_elephant(&map, &mut cache, "AA", "AA", 0, 0, 26).to_string()
}
