use std::collections::HashSet;

struct Rock {
    width: i64,
    height: i64,
    points: Vec<(i64, i64)>,
}

impl Rock {
    pub fn collides(&self, reference: &(i64, i64), placed_rocks: &HashSet<(i64, i64)>) -> bool {
        if reference.0 < 0 || reference.0 > 6 || reference.1 < 0 {
            return true;
        }

        if self
            .points
            .iter()
            .any(|(x, y)| placed_rocks.contains(&(x + reference.0, y + reference.1)))
        {
            return true;
        }

        false
    }

    pub fn push(
        &self,
        reference: (i64, i64),
        placed_rocks: &HashSet<(i64, i64)>,
        direction: char,
    ) -> (i64, i64) {
        let new_reference = match direction {
            '>' => {
                if reference.0 + self.width < 7 {
                    (reference.0 + 1, reference.1)
                } else {
                    (reference.0, reference.1)
                }
            }
            '<' => {
                if reference.0 > 0 {
                    (reference.0 - 1, reference.1)
                } else {
                    (reference.0, reference.1)
                }
            }
            _ => panic!("Wrong air direction"),
        };

        if self.collides(&new_reference, placed_rocks) {
            return reference;
        }
        new_reference
    }

    pub fn fall(&self, reference: (i64, i64), placed_rocks: &HashSet<(i64, i64)>) -> (i64, i64) {
        let new_reference = (reference.0, reference.1 - 1);
        if self.collides(&new_reference, placed_rocks) {
            return reference;
        }
        new_reference
    }
}

fn get_height_after_steps(input: &str, steps: usize) -> i64 {
    let rocks = vec![
        Rock {
            width: 4,
            height: 1,
            points: vec![(0, 0), (1, 0), (2, 0), (3, 0)],
        },
        Rock {
            width: 3,
            height: 3,
            points: vec![(1, 0), (0, 1), (1, 1), (2, 1), (1, 2)],
        },
        Rock {
            width: 3,
            height: 3,
            points: vec![(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)],
        },
        Rock {
            width: 1,
            height: 4,
            points: vec![(0, 0), (0, 1), (0, 2), (0, 3)],
        },
        Rock {
            width: 2,
            height: 2,
            points: vec![(0, 0), (0, 1), (1, 0), (1, 1)],
        },
    ];

    let mut total_height = 0;
    let mut fallen_rocks = 0;
    let mut reference = (2, 3);
    let mut placed_rocks = HashSet::new();
    let mut start_of_cycle = None;
    let mut cycle: Vec<((usize, usize), i64)> = Vec::new();
    let duration_cycle_jet = input.trim().len();

    for (i, direction) in input.trim().chars().cycle().enumerate() {
        if fallen_rocks == steps {
            break;
        }

        let current_rock = &rocks[fallen_rocks % rocks.len()];

        reference = current_rock.push(reference, &placed_rocks, direction);

        let reference_after_fall = current_rock.fall(reference, &placed_rocks);

        if reference == reference_after_fall {
            placed_rocks.extend(
                current_rock
                    .points
                    .iter()
                    .map(|(x, y)| (x + reference.0, y + reference.1)),
            );
            fallen_rocks += 1;
            total_height = total_height.max(reference.1 + current_rock.height);
            reference = (2, 3 + total_height);
            if fallen_rocks > 20000 {
                let rock_index = fallen_rocks % rocks.len();
                let jet_index = i % duration_cycle_jet;
                if let Some(start) = start_of_cycle {
                    let current = (rock_index, jet_index);
                    if current == start {
                        let fallen_rocks_per_cycle = total_height - cycle[0].1;
                        let full_cycles = ((steps - fallen_rocks) / cycle.len()) as i64;
                        let remainder = (steps - fallen_rocks) % cycle.len();

                        total_height += full_cycles * fallen_rocks_per_cycle;
                        for j in 0..remainder {
                            total_height += cycle[j + 1].1 - cycle[j].1;
                        }

                        break;
                    } else {
                        cycle.push((current, total_height));
                    }
                } else {
                    start_of_cycle = Some((rock_index, jet_index));
                    cycle.push(((rock_index, jet_index), total_height));
                }
            }
        } else {
            reference = reference_after_fall;
        }
    }
    total_height
}

pub fn part1(input: &str) -> String {
    get_height_after_steps(input, 2022).to_string()
}

pub fn part2(input: &str) -> String {
    get_height_after_steps(input, 1000000000000).to_string()
}
