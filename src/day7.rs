use std::collections::HashMap;

struct Command {
    args: Vec<String>,
    output: Vec<String>,
}

impl Command {
    pub fn new() -> Command {
        Command {
            args: Vec::new(),
            output: Vec::new(),
        }
    }
}

fn parse(input: &str) -> Vec<Command> {
    let mut result: Vec<Command> = Vec::new();

    for line in input.split('\n') {
        if line.is_empty() {
            continue;
        }
        if line.starts_with('$') {
            result.push(Command::new());
            for word in line.split_whitespace() {
                if word == "$" {
                    continue;
                }

                match result.last_mut() {
                    Some(r) => r.args.push(word.into()),
                    None => println!("Empty vector?"),
                }
            }
        } else {
            match result.last_mut() {
                Some(r) => r.output.push(line.into()),
                None => println!("Empty vector?"),
            }
        }
    }

    result
}

type DirTree = HashMap<String, Vec<(String, i32)>>;

fn create_tree(commands: Vec<Command>) -> DirTree {
    let mut tree: DirTree = HashMap::new();

    let mut current_dir: Vec<String> = Vec::new();
    for cmd in commands {
        match cmd.args.first().expect("Empty command").as_str() {
            "cd" => {
                let dir = cmd.args.last().expect("Empty vec");

                if dir == ".." {
                    current_dir.pop();
                } else if dir != "/" {
                    current_dir.push(dir.to_string() + "/");
                } else {
                    current_dir.push(dir.to_string());
                }
            }
            "ls" => {
                for line in cmd.output {
                    let line_split: Vec<&str> = line.split_whitespace().collect();

                    let size = line_split.first().expect("Empty vec").parse().unwrap_or(0);
                    let name = line_split.last().expect("Empty vec").to_string();

                    let joined_dir = current_dir.join("");

                    if let std::collections::hash_map::Entry::Vacant(e) =
                        tree.entry(joined_dir.clone())
                    {
                        e.insert(vec![(name, size)]);
                    } else if let Some(v) = tree.get_mut(&joined_dir) {
                        v.push((name, size));
                    }
                }
            }
            _ => println!("Unrecognized command"),
        }
    }

    tree
}

fn calculate_size_dir(tree: &DirTree, dir: &str) -> i32 {
    let mut total_size = 0;
    for (name, size) in tree.get(dir).expect("Key not found") {
        if *size == 0 {
            let new_dir = format!("{}{}/", dir, name);
            total_size += calculate_size_dir(tree, &new_dir);
        } else {
            total_size += *size;
        }
    }
    total_size
}

fn get_dir_sizes(tree: &DirTree) -> Vec<i32> {
    let mut sizes: Vec<i32> = Vec::new();
    for key in tree.keys() {
        sizes.push(calculate_size_dir(tree, key));
    }
    sizes
}

pub fn part1(input: &str) -> String {
    let tree = create_tree(parse(input));
    let sizes = get_dir_sizes(&tree);

    let mut result = 0;
    for v in sizes {
        if v < 100000 {
            result += v;
        }
    }

    result.to_string()
}

pub fn part2(input: &str) -> String {
    let tree = create_tree(parse(input));
    let mut sizes = get_dir_sizes(&tree);
    sizes.sort();

    let total_size = 70000000;
    let used_size: i32 = calculate_size_dir(&tree, "/");
    let needed_space = 30000000 - (total_size - used_size);

    let mut result = 0;
    for v in sizes {
        if v >= needed_space {
            result = v;
            break;
        }
    }

    result.to_string()
}
