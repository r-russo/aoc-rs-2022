#[derive(Debug)]
struct Registers {
    pc: i32,
    x: i32,
    signal_strength: i32,
    screen: String,
}

fn calculate_signal_strength(registers: &Registers) -> i32 {
    if registers.pc == 20 || (registers.pc + 20) % 40 == 0 {
        registers.pc * registers.x
    } else {
        0
    }
}

fn increase_cycles(mut registers: Registers, update_screen: bool) -> Registers {
    registers.signal_strength += calculate_signal_strength(&registers);

    if update_screen {
        let column = (registers.pc - 1) % 40;
        if column == 0 {
            registers.screen += "\n";
        }
        if column <= registers.x + 1 && column >= registers.x - 1 {
            registers.screen += "█";
        } else {
            registers.screen += " ";
        }

        // println!("During cycle {}:", registers.pc);
        // println!("Sprite position: {}", registers.x);
        // println!("Current column: {}", column);
        // println!("Current row: {}", registers.screen);
    }

    registers.pc += 1;
    registers
}

fn execute_program(program: &str, update_screen: bool) -> Registers {
    let mut registers = Registers {
        pc: 1,
        x: 1,
        signal_strength: 0,
        screen: String::new(),
    };

    for instruction in program.split('\n') {
        let mut it = instruction.split_whitespace();

        let command = it.next().unwrap();
        match command {
            "addx" => {
                let value: i32 = it.next().unwrap().parse().unwrap();
                registers = increase_cycles(registers, update_screen);
                registers = increase_cycles(registers, update_screen);
                registers.x += value;
            }

            "noop" => {
                registers = increase_cycles(registers, update_screen);
            }
            _ => println!("Unrecognized command {}", command),
        }
    }

    registers
}

pub fn part1(input: &str) -> String {
    execute_program(input.trim(), false)
        .signal_strength
        .to_string()
}

pub fn part2(input: &str) -> String {
    execute_program(input.trim(), true).screen
}
