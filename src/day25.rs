fn snafu_to_decimal(snafu: &str) -> i64 {
    let mut result = 0;
    for (i, d) in snafu.chars().rev().enumerate() {
        result += 5_i64.pow(i.try_into().unwrap())
            * match d {
                '2' => 2,
                '1' => 1,
                '0' => 0,
                '-' => -1,
                '=' => -2,
                _ => panic!("Unexpected digit {}", d),
            };
    }

    result
}

fn decimal_to_snafu(mut decimal: i64) -> String {
    let mut snafu = String::new();
    while decimal > 0 {
        let digit = decimal % 5;
        let mut remainder = 0;

        snafu.push(match digit {
            0 => '0',
            1 => '1',
            2 => '2',
            3 => {
                remainder = 1;
                '='
            }
            4 => {
                remainder = 1;
                '-'
            }
            _ => panic!("Unexpected digit {}", digit),
        });

        decimal /= 5;
        decimal += remainder;
    }
    snafu.chars().rev().collect()
}

pub fn part1(input: &str) -> String {
    let mut sum = 0;
    for num in input.trim().split('\n') {
        sum += snafu_to_decimal(num);
    }

    decimal_to_snafu(sum)
}

pub fn part2(_: &str) -> String {
    0.to_string()
}
