use std::collections::HashSet;

#[derive(Debug, Hash, Eq, PartialEq)]
struct Point {
    x: i32,
    y: i32,
}

impl From<&str> for Point {
    fn from(point: &str) -> Self {
        let p = point.split_once(',').unwrap();
        Point {
            x: p.0.parse().unwrap(),
            y: p.1.parse().unwrap(),
        }
    }
}

fn make_straight_line(p1: &Point, p2: &Point) -> HashSet<Point> {
    if p1.x == p2.x {
        // vertical
        if p2.y > p1.y {
            (p1.y..p2.y + 1).map(|y| Point { x: p1.x, y }).collect()
        } else {
            (p2.y..p1.y + 1).map(|y| Point { x: p1.x, y }).collect()
        }
    } else if p1.y == p2.y {
        // horizontal
        if p2.x > p1.x {
            (p1.x..p2.x + 1).map(|x| Point { x, y: p1.y }).collect()
        } else {
            (p2.x..p1.x + 1).map(|x| Point { x, y: p1.y }).collect()
        }
    } else {
        panic!("Invalid points");
    }
}

fn get_path_of_rocks(input: &str) -> HashSet<Point> {
    let mut rocks = HashSet::new();

    for line in input.trim().split('\n') {
        let points_iter = line.split("->");
        for (previous, current) in points_iter.clone().zip(points_iter.skip(1)) {
            let p1 = Point::from(previous.trim());
            let p2 = Point::from(current.trim());
            rocks.extend(make_straight_line(&p1, &p2));
        }
    }

    rocks
}

fn get_farthest_down(rocks: &HashSet<Point>, sand: &Point, floor: i32) -> Point {
    let mut bottom = Point {
        x: sand.x,
        y: sand.y,
    };

    loop {
        let next = Point {
            x: bottom.x,
            y: bottom.y + 1,
        };

        if next.y == floor || rocks.contains(&next) {
            break;
        }

        bottom = next;
    }

    bottom
}

fn create_unit_of_sand(rocks: &HashSet<Point>, starting_sand: &Point, floor: i32) -> Point {
    let bottom_sand = get_farthest_down(rocks, starting_sand, floor);
    let left = Point {
        x: bottom_sand.x - 1,
        y: bottom_sand.y + 1,
    };
    let right = Point {
        x: bottom_sand.x + 1,
        y: bottom_sand.y + 1,
    };

    if !rocks.contains(&left) && bottom_sand.y < floor - 1 {
        create_unit_of_sand(rocks, &left, floor)
    } else if !rocks.contains(&right) && bottom_sand.y < floor - 1 {
        create_unit_of_sand(rocks, &right, floor)
    } else {
        bottom_sand
    }
}

pub fn part1(input: &str) -> String {
    let mut path_of_rocks = get_path_of_rocks(input);
    let floor = path_of_rocks.iter().map(|p| p.y).max().unwrap() + 1;

    for i in 0.. {
        let sand = create_unit_of_sand(&path_of_rocks, &Point { x: 500, y: 0 }, floor);

        if sand.y == floor - 1 {
            return i.to_string();
        }

        path_of_rocks.insert(sand);
    }

    0.to_string()
}

pub fn part2(input: &str) -> String {
    let mut path_of_rocks = get_path_of_rocks(input);
    let goal = Point { x: 500, y: 0 };
    let floor = path_of_rocks.iter().map(|p| p.y).max().unwrap() + 2;

    for i in 0.. {
        let sand = create_unit_of_sand(&path_of_rocks, &Point { x: 500, y: 0 }, floor);

        if sand == goal {
            return (i + 1).to_string();
        }

        path_of_rocks.insert(sand);
    }

    0.to_string()
}
