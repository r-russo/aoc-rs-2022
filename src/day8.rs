use std::collections::HashSet;

#[derive(Debug)]
struct Matrix {
    content: Vec<i32>,
    rows: usize,
    cols: usize,
}

fn parse(input: &str) -> Matrix {
    let mut trees = Matrix {
        content: Vec::new(),
        rows: 0,
        cols: 0,
    };

    for line in input.trim().split('\n') {
        trees.rows += 1;
        if trees.cols == 0 {
            trees.cols = line.len();
        }
        for c in line.chars() {
            trees
                .content
                .push(c.to_digit(10).expect("Not a base 10 digit") as i32);
        }
    }

    trees
}

fn find_visible(trees: &Matrix) -> i32 {
    let mut visited: HashSet<(usize, usize)> = HashSet::new();

    // left
    for i in 1..trees.rows - 1 {
        let mut max_height = *trees.content.get(i * trees.cols).expect("Invalid index");
        for j in 1..trees.cols - 1 {
            let current = *trees
                .content
                .get(j + i * trees.cols)
                .expect("Invalid index");

            if max_height < current {
                max_height = current;
                visited.insert((i, j));
                if max_height == 9 {
                    break;
                }
            }
        }
    }

    // top
    for j in 1..trees.cols - 1 {
        let mut max_height = *trees.content.get(j).expect("Invalid index");
        for i in 1..trees.rows - 1 {
            let current = *trees
                .content
                .get(j + i * trees.cols)
                .expect("Invalid index");

            if max_height < current {
                max_height = current;
                visited.insert((i, j));
                if max_height == 9 {
                    break;
                }
            }
        }
    }

    // right
    for i in 1..trees.rows - 1 {
        let mut max_height = *trees
            .content
            .get((i + 1) * trees.cols - 1)
            .expect("Invalid index");
        for j in (1..trees.cols - 1).rev() {
            let current = *trees
                .content
                .get(j + i * trees.cols)
                .expect("Invalid index");

            if max_height < current {
                max_height = current;
                visited.insert((i, j));
                if max_height == 9 {
                    break;
                }
            }
        }
    }

    for j in 1..trees.cols - 1 {
        let mut max_height = *trees
            .content
            .get(trees.content.len() - trees.cols + j)
            .expect("Invalid index");
        for i in (1..trees.rows - 1).rev() {
            let current = *trees
                .content
                .get(j + i * trees.cols)
                .expect("Invalid index");

            if max_height < current {
                max_height = current;
                visited.insert((i, j));
                if max_height == 9 {
                    break;
                }
            }
        }
    }

    for i in 0..trees.rows {
        visited.insert((i, 0));
        visited.insert((i, trees.cols - 1));
    }

    for j in 0..trees.cols {
        visited.insert((0, j));
        visited.insert((trees.rows - 1, j));
    }

    visited.len() as i32
}

fn scenic_score(trees: &Matrix, row: usize, col: usize) -> i32 {
    let mut score = 1;

    // to right
    let max_height = *trees
        .content
        .get(col + row * trees.cols)
        .expect("Invalid index");
    let mut distance = 0;
    for j in col + 1..trees.cols {
        let current = *trees
            .content
            .get(j + row * trees.cols)
            .expect("Invalid index");

        if max_height > current {
            distance += 1;
        } else {
            distance += 1;
            break;
        }
    }
    score *= distance;

    // to left
    let max_height = *trees
        .content
        .get(col + row * trees.cols)
        .expect("Invalid index");
    let mut distance = 0;
    for j in (0..col).rev() {
        let current = *trees
            .content
            .get(j + row * trees.cols)
            .expect("Invalid index");

        if max_height > current {
            distance += 1;
        } else {
            distance += 1;
            break;
        }
    }
    score *= distance;

    // to top
    let max_height = *trees
        .content
        .get(col + row * trees.cols)
        .expect("Invalid index");
    let mut distance = 0;
    for i in (0..row).rev() {
        let current = *trees
            .content
            .get(col + i * trees.cols)
            .expect("Invalid index");

        if max_height > current {
            distance += 1;
        } else {
            distance += 1;
            break;
        }
    }
    score *= distance;

    // to bottom
    let max_height = *trees
        .content
        .get(col + row * trees.cols)
        .expect("Invalid index");
    let mut distance = 0;
    for i in row + 1..trees.rows {
        let current = *trees
            .content
            .get(col + i * trees.cols)
            .expect("Invalid index");

        if max_height > current {
            distance += 1;
        } else {
            distance += 1;
            break;
        }
    }
    score *= distance;

    score
}

pub fn part1(input: &str) -> String {
    let trees = parse(input);
    find_visible(&trees).to_string()
}

pub fn part2(input: &str) -> String {
    let trees = parse(input);

    let mut max_score = 0;
    for i in 1..trees.rows - 1 {
        for j in 1..trees.cols - 1 {
            let score = scenic_score(&trees, i, j);

            if score > max_score {
                max_score = score
            }
        }
    }

    max_score.to_string()
}
