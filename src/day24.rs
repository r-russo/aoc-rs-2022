use std::collections::{HashSet, VecDeque};

type Position = (usize, usize);

struct Map {
    content: Vec<Vec<char>>,
    w: usize,
    h: usize,
}

impl Map {
    pub fn get(&self, t: usize, p: &Position) -> char {
        let x = p.0;
        let y = p.1;
        if let Some(&c) = self.content[t].get(y * self.w + x) {
            c
        } else {
            '#'
        }
    }
}

fn update_blizzards(
    blizzards: Vec<(Position, char)>,
    map: &[char],
    w: usize,
    h: usize,
) -> Vec<(Position, char)> {
    let mut new_blizzards = Vec::new();
    for (position, direction) in blizzards {
        let mut next_position = match direction {
            '>' => (position.0 + 1, position.1),
            '<' => (position.0 - 1, position.1),
            '^' => (position.0, position.1 - 1),
            'v' => (position.0, position.1 + 1),
            _ => panic!("Wrong blizzard direction"),
        };

        if map[next_position.1 * w + next_position.0] == '#' {
            next_position = match direction {
                '>' => (1, next_position.1),
                '<' => (w - 2, next_position.1),
                '^' => (next_position.0, h - 2),
                'v' => (next_position.0, 1),
                _ => panic!("Wrong blizzard direction"),
            };
        }

        new_blizzards.push((next_position, direction));
    }

    new_blizzards
}

fn parse(input: &str) -> Map {
    let mut base_grid = Vec::new();
    let mut blizzards = Vec::new();
    let mut w = 0;
    let mut h = 0;

    for (i, line) in input.trim().split('\n').enumerate() {
        for (j, c) in line.chars().enumerate() {
            if !['#', '.'].contains(&c) {
                blizzards.push(((j, i), c));
                base_grid.push('.');
            } else {
                base_grid.push(c);
            }
            w = j + 1;
        }
        h = i + 1;
    }

    let mut time_grid = Vec::new();

    for _ in 0..w * h {
        let mut current_grid = base_grid.clone();

        for (pos, c) in blizzards.clone() {
            current_grid[pos.1 * w + pos.0] = c;
        }

        time_grid.push(current_grid);
        blizzards = update_blizzards(blizzards, &base_grid, w, h);
    }

    Map {
        content: time_grid,
        w,
        h,
    }
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
struct State {
    position: Position,
    time: usize,
}

// impl PartialOrd for State {
//     fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
//         Some(self.cmp(other))
//     }
// }
//
// impl Ord for State {
//     fn cmp(&self, other: &Self) -> Ordering {
//         other
//             .distance
//             .cmp(&self.distance)
//             .then_with(|| other.time.cmp(&self.time))
//     }
// }

fn get_neighbours(t: usize, p: Position, map: &Map) -> Vec<(usize, Position)> {
    let mut neighbours = Vec::new();
    let new_time = (t + 1) % map.content.len();

    if map.get(new_time, &p) == '.' {
        neighbours.push((new_time, p));
    }

    let next = (p.0 + 1, p.1);
    if map.get(new_time, &next) == '.' {
        neighbours.push((new_time, next));
    }

    if p.0 > 0 {
        let next = (p.0 - 1, p.1);
        if map.get(new_time, &next) == '.' {
            neighbours.push((new_time, next));
        }
    }

    let next = (p.0, p.1 + 1);
    if map.get(new_time, &next) == '.' {
        neighbours.push((new_time, next));
    }

    if p.1 > 0 {
        let next = (p.0, p.1 - 1);
        if map.get(new_time, &next) == '.' {
            neighbours.push((new_time, next));
        }
    }

    neighbours
}

fn bfs(map: &Map, initial_state: State, goal: Position) -> (usize, usize) {
    let mut queue = VecDeque::new();

    let start = initial_state;
    let mut visited = HashSet::new();
    visited.insert(start);
    queue.push_back((0, start));

    while let Some((distance, State { position, time })) = queue.pop_front() {
        // println!("{} {:?}", distance, State { position, time });
        if position == goal {
            return (distance, time);
        }

        for (t, p) in get_neighbours(time, position, map) {
            let next_state = State {
                position: p,
                time: t,
            };

            if visited.contains(&next_state) {
                continue;
            }

            visited.insert(next_state);
            if map.get(next_state.time, &next_state.position) == '.' {
                queue.push_back((distance + 1, next_state));
            }
        }
    }

    (0, 0)
}

pub fn part1(input: &str) -> String {
    let map = parse(input);

    bfs(
        &map,
        State {
            position: (1, 0),
            time: 0,
        },
        (map.w - 2, map.h - 1),
    )
    .0
    .to_string()
}

pub fn part2(input: &str) -> String {
    let map = parse(input);
    let mut total_distance = 0;
    let start = (1, 0);
    let end = (map.w - 2, map.h - 1);

    let (distance, time) = bfs(
        &map,
        State {
            position: start,
            time: 0,
        },
        end,
    );
    total_distance += distance;

    let (distance, time) = bfs(
        &map,
        State {
            position: end,
            time,
        },
        start,
    );
    total_distance += distance;

    let (distance, _) = bfs(
        &map,
        State {
            position: start,
            time,
        },
        end,
    );
    total_distance += distance;

    total_distance.to_string()
}
