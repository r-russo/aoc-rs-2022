use std::collections::HashSet;

#[derive(Debug, Hash, Clone, Copy, PartialEq, Eq)]
struct Position {
    x: i64,
    y: i64,
}

impl From<&str> for Position {
    fn from(point: &str) -> Self {
        let (x_string, y_string) = point.split_once(", ").unwrap();
        Position {
            x: x_string[2..].parse().unwrap(),
            y: y_string[2..].parse().unwrap(),
        }
    }
}

fn distance(p1: &Position, p2: &Position) -> i64 {
    (p1.x - p2.x).abs() + (p1.y - p2.y).abs()
}

fn parse(input: &str) -> Vec<(Position, Position)> {
    let mut sensors_and_beacons = Vec::new();
    for line in input.trim().split('\n') {
        if let Some((sensor_string, beacon_string)) = line.split_once(": ") {
            let position_x_sensors = sensor_string.find("x=").unwrap();
            let position_x_beacon = beacon_string.find("x=").unwrap();
            sensors_and_beacons.push((
                Position::from(&sensor_string[position_x_sensors..]),
                Position::from(&beacon_string[position_x_beacon..]),
            ));
        }
    }
    sensors_and_beacons.sort_by(|(s1, b1), (s2, b2)| {
        let d1 = distance(s1, b1);
        let d2 = distance(s2, b2);
        d1.cmp(&d2)
    });
    sensors_and_beacons.reverse();
    sensors_and_beacons
}

fn get_extremes(sensors_and_beacons: &Vec<(Position, Position)>) -> (Position, Position) {
    let mut top_left = Position { x: 0, y: 0 };
    let mut bottom_right = Position { x: 0, y: 0 };
    for (sensor, beacon) in sensors_and_beacons {
        let d = distance(sensor, beacon);
        if sensor.x - d < top_left.x {
            top_left.x = sensor.x - d;
        } else if sensor.x + d > bottom_right.x {
            bottom_right.x = sensor.x + d;
        }

        if sensor.y - d < top_left.y {
            top_left.y = sensor.y - d;
        } else if sensor.y + d > bottom_right.y {
            bottom_right.y = sensor.y + d;
        }
    }

    (top_left, bottom_right)
}

fn count_not_beacons_in_row(
    sensors_and_beacons: &Vec<(Position, Position)>,
    row: i64,
    mut left: i64,
    right: i64,
) -> (i64, Option<Position>) {
    let mut count = 0;
    let mut beacon_position = None;

    while left <= right {
        let mut found_region = false;
        let point = Position { x: left, y: row };
        for (sensor, beacon) in sensors_and_beacons {
            let r = distance(sensor, beacon);
            let distance_sensor_point = distance(sensor, &point);
            if distance_sensor_point <= r {
                let d = sensor.x - point.x;
                let mut delta = 0;
                if d >= 0 {
                    delta += d * 2 + 1;
                } else if d < 0 {
                    delta += r + d - (sensor.y - point.y).abs() + 1;
                }
                count += delta;
                left += delta;
                found_region = true;
                break;
            }
        }

        if !found_region {
            beacon_position = Some(point);
            left += 1;
        }
    }

    count -= sensors_and_beacons
        .iter()
        .filter(|(_, b)| b.y == row)
        .map(|(_, b)| b.x)
        .collect::<HashSet<i64>>()
        .len() as i64;

    (count, beacon_position)
}

pub fn part1(input: &str) -> String {
    let sensors_and_beacons = parse(input);
    let (top_left, bottom_right) = get_extremes(&sensors_and_beacons);
    count_not_beacons_in_row(&sensors_and_beacons, 2000000, top_left.x, bottom_right.x)
        .0
        .to_string()
}

pub fn part2(input: &str) -> String {
    let sensors_and_beacons = parse(input);
    for row in 0..4000000 {
        if let (_, Some(b)) = count_not_beacons_in_row(&sensors_and_beacons, row, 0, 4000000) {
            return (b.x * 4000000 + b.y).to_string();
        }
    }
    0.to_string()
}
