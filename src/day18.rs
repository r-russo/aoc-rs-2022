use std::collections::HashSet;

fn parse(input: &str) -> HashSet<(i32, i32, i32)> {
    let mut cubes = HashSet::new();
    for line in input.trim().split('\n') {
        let (x, yz) = line.split_once(',').unwrap();
        let (y, z) = yz.split_once(',').unwrap();
        cubes.insert((x.parse().unwrap(), y.parse().unwrap(), z.parse().unwrap()));
    }

    cubes
}

fn count_free_faces(
    cube: &(i32, i32, i32),
    cubes: &HashSet<(i32, i32, i32)>,
    air_outside: Option<&HashSet<(i32, i32, i32)>>,
) -> i32 {
    let mut count = 0;
    let (x, y, z) = cube;
    let neighbors = vec![
        (1, 0, 0),
        (0, 1, 0),
        (0, 0, 1),
        (-1, 0, 0),
        (0, -1, 0),
        (0, 0, -1),
    ];

    for (dx, dy, dz) in neighbors {
        let next = (x + dx, y + dy, z + dz);
        if air_outside.is_some() && !air_outside.unwrap().contains(&next) {
            continue;
        }
        if !cubes.contains(&next) {
            count += 1;
        }
    }
    count
}

fn count_adjacent(cubes: &HashSet<(i32, i32, i32)>) -> i32 {
    let mut count = 0;

    for c in cubes.iter() {
        count += count_free_faces(c, cubes, None);
    }

    count
}

fn count_adjacent_with_air_pockets(cubes: &HashSet<(i32, i32, i32)>) -> i32 {
    let (mut minx, mut maxx, mut miny, mut maxy, mut minz, mut maxz) = (0, 0, 0, 0, 0, 0);

    for c in cubes.iter() {
        minx = minx.min(c.0);
        maxx = maxx.max(c.0);
        miny = miny.min(c.1);
        maxy = maxy.max(c.1);
        minz = minz.min(c.2);
        maxz = maxz.max(c.2);
    }

    let mut stack = Vec::new();
    let mut air_outside = HashSet::new();
    stack.push((minx, miny, minz));

    while let Some((x, y, z)) = stack.pop() {
        let neighbors = vec![
            (1, 0, 0),
            (0, 1, 0),
            (0, 0, 1),
            (-1, 0, 0),
            (0, -1, 0),
            (0, 0, -1),
        ];

        for (dx, dy, dz) in neighbors {
            let next = (x + dx, y + dy, z + dz);
            if next.0 < minx - 1
                || next.1 < miny - 1
                || next.2 < minz - 1
                || next.0 > maxx + 1
                || next.1 > maxy + 1
                || next.2 > maxz + 1
            {
                continue;
            }

            if !cubes.contains(&next) && !air_outside.contains(&next) {
                stack.push(next);
                air_outside.insert(next);
            }
        }
    }

    let mut count = 0;

    for c in cubes.iter() {
        count += count_free_faces(c, cubes, Some(&air_outside));
    }
    count
}

pub fn part1(input: &str) -> String {
    let cubes = parse(input);
    count_adjacent(&cubes).to_string()
}

pub fn part2(input: &str) -> String {
    let cubes = parse(input);

    count_adjacent_with_air_pockets(&cubes).to_string()
}
