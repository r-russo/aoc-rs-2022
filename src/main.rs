use std::env;
use std::io::{self, Read};
use std::time::Instant;

mod day1;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day2;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut input = String::new();
    io::stdin()
        .read_to_string(&mut input)
        .expect("Failed to read input");

    if args.len() != 2 {
        panic!("Wrong number of parameters. Expected 1");
    }

    let now = Instant::now();

    let day: i32 = args[1].parse().expect("Parameter must be a number");

    let (r1, r2) = match day {
        1 => (day1::part1(&input), day1::part2(&input)),
        2 => (day2::part1(&input), day2::part2(&input)),
        3 => (day3::part1(&input), day3::part2(&input)),
        4 => (day4::part1(&input), day4::part2(&input)),
        5 => (day5::part1(&input), day5::part2(&input)),
        6 => (day6::part1(&input), day6::part2(&input)),
        7 => (day7::part1(&input), day7::part2(&input)),
        8 => (day8::part1(&input), day8::part2(&input)),
        9 => (day9::part1(&input), day9::part2(&input)),
        10 => (day10::part1(&input), day10::part2(&input)),
        11 => (day11::part1(&input), day11::part2(&input)),
        12 => (day12::part1(&input), day12::part2(&input)),
        13 => (day13::part1(&input), day13::part2(&input)),
        14 => (day14::part1(&input), day14::part2(&input)),
        15 => (day15::part1(&input), day15::part2(&input)),
        16 => (day16::part1(&input), day16::part2(&input)),
        17 => (day17::part1(&input), day17::part2(&input)),
        18 => (day18::part1(&input), day18::part2(&input)),
        19 => (day19::part1(&input), day19::part2(&input)),
        20 => (day20::part1(&input), day20::part2(&input)),
        21 => (day21::part1(&input), day21::part2(&input)),
        22 => (day22::part1(&input), day22::part2(&input)),
        23 => (day23::part1(&input), day23::part2(&input)),
        24 => (day24::part1(&input), day24::part2(&input)),
        25 => (day25::part1(&input), day25::part2(&input)),
        _ => panic!("Invalid day"),
    };

    let elapsed = now.elapsed();

    println!("Result day {}:", day);
    println!("  Part 1: {}", r1);
    println!("  Part 2: {}", r2);
    println!("Elapsed: {:.2?}", elapsed);
}
