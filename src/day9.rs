use std::collections::HashSet;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

fn init_rope(length: i32) -> Vec<Point> {
    let mut rope: Vec<Point> = Vec::new();

    for _ in 0..length {
        rope.push(Point { x: 0, y: 0 });
    }

    rope
}

fn move_rope(rope: &mut Vec<Point>, direction: Point) -> Point {
    rope[0] = Point {
        x: rope[0].x + direction.x,
        y: rope[0].y + direction.y,
    };

    for i in 1..rope.len() {
        let next = *rope.get(i - 1).unwrap();
        let current = *rope.get(i).unwrap();

        if (next.x - current.x).abs() + (next.y - current.y).abs() >= 3 {
            rope[i].x += (next.x - current.x).signum();
            rope[i].y += (next.y - current.y).signum();
        } else if (next.x - current.x).abs() == 2 {
            rope[i].x += (next.x - current.x).signum();
        } else if (next.y - current.y).abs() == 2 {
            rope[i].y += (next.y - current.y).signum();
        }
    }

    *rope.last().unwrap()
}

fn update_rope(rope: &mut Vec<Point>, visited_by_tail: &mut HashSet<Point>, instruction: &str) {
    let mut it = instruction.split_whitespace();
    let direction: &str = it.next().expect("Empty line");
    let value: i32 = it
        .next()
        .expect("Empty line")
        .parse()
        .expect("Not a number");

    for _ in 0..value {
        match direction {
            "L" => visited_by_tail.insert(move_rope(rope, Point { x: -1, y: 0 })),
            "R" => visited_by_tail.insert(move_rope(rope, Point { x: 1, y: 0 })),
            "U" => visited_by_tail.insert(move_rope(rope, Point { x: 0, y: 1 })),
            "D" => visited_by_tail.insert(move_rope(rope, Point { x: 0, y: -1 })),
            _ => false,
        };
    }
}

pub fn part1(input: &str) -> String {
    let mut rope = init_rope(2);
    let mut visited_tail: HashSet<Point> = HashSet::new();

    for line in input.trim().split('\n') {
        update_rope(&mut rope, &mut visited_tail, line);
    }

    visited_tail.len().to_string()
}

pub fn part2(input: &str) -> String {
    let mut rope = init_rope(10);
    let mut visited_tail: HashSet<Point> = HashSet::new();

    for line in input.trim().split('\n') {
        update_rope(&mut rope, &mut visited_tail, line);
    }

    visited_tail.len().to_string()
}
