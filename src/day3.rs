use std::collections::HashSet;

fn get_priority_value(x: char) -> Option<i32> {
    let val = x as i32;

    if (97..=122).contains(&val) {
        Some(val - 96)
    } else if (65..=90).contains(&val) {
        Some(val - 64 + 26)
    } else {
        None
    }
}

fn parse_rucksack(line: &str) -> i32 {
    let mut first: HashSet<char> = HashSet::new();
    let mut second: HashSet<char> = HashSet::new();
    let n = line.len();

    for (i, c) in line.chars().enumerate() {
        if i < n / 2 {
            first.insert(c);
        } else {
            second.insert(c);
        }
    }

    let mut priority_sum = 0;
    for x in first.intersection(&second) {
        priority_sum += get_priority_value(*x).expect("Not valid");
    }

    priority_sum
}

fn parse_groups(group1: &str, group2: &str, group3: &str) -> i32 {
    let first: HashSet<char> = group1.chars().collect();
    let second: HashSet<char> = group2.chars().collect();
    let third: HashSet<char> = group3.chars().collect();

    let mut priority_sum = 0;
    for x in first.intersection(&second) {
        if third.contains(x) {
            priority_sum += get_priority_value(*x).expect("Not valid");
        }
    }

    priority_sum
}

pub fn part1(input: &str) -> String {
    let mut priority_sum = 0;
    for line in input.split('\n') {
        priority_sum += parse_rucksack(line);
    }
    priority_sum.to_string()
}

pub fn part2(input: &str) -> String {
    let mut priority_sum = 0;
    let mut lines = input.trim().split('\n').peekable();
    while lines.peek().is_some() {
        priority_sum += parse_groups(
            lines.next().expect("Can't get first group"),
            lines.next().expect("Can't get second group"),
            lines.next().expect("Can't get third group"),
        );
    }
    priority_sum.to_string()
}
