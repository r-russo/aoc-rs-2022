use std::collections::HashMap;

#[derive(Debug)]
enum Direction {
    Forward(i32),
    RotateLeft,
    RotateRight,
}

fn parse(input: &str) -> (HashMap<(i32, i32), char>, Vec<Direction>) {
    let mut map = HashMap::new();
    let mut path = Vec::new();

    let (map_str, path_str) = input.split_once("\n\n").unwrap();
    for (i, line) in map_str.split('\n').enumerate() {
        for (j, col) in line.chars().enumerate() {
            match col {
                '#' => map.insert((j.try_into().unwrap(), i.try_into().unwrap()), col),
                '.' => map.insert((j.try_into().unwrap(), i.try_into().unwrap()), col),
                _ => None,
            };
        }
    }

    let mut current_move = "".to_string();
    for c in path_str.trim().chars() {
        if c.is_alphabetic() {
            path.push(Direction::Forward(current_move.parse().unwrap()));
            match c {
                'L' => path.push(Direction::RotateLeft),
                'R' => path.push(Direction::RotateRight),
                _ => (),
            }
            current_move = "".to_string();
        } else {
            current_move.push(c);
        }
    }

    if !current_move.is_empty() {
        path.push(Direction::Forward(current_move.parse().unwrap()));
    }

    (map, path)
}

fn wrap(
    position: (i32, i32),
    direction: (i32, i32),
    map: &HashMap<(i32, i32), char>,
) -> ((i32, i32), (i32, i32)) {
    match direction {
        (1, 0) => {
            for x in (0..position.0).rev() {
                if !map.contains_key(&(x - direction.0, position.1)) {
                    return ((x, position.1), direction);
                }
            }
        }
        (-1, 0) => {
            for x in position.0.. {
                if !map.contains_key(&(x - direction.0, position.1)) {
                    return ((x, position.1), direction);
                }
            }
        }
        (0, 1) => {
            for y in (0..position.1).rev() {
                if !map.contains_key(&(position.0, y - direction.1)) {
                    return ((position.0, y), direction);
                }
            }
        }
        (0, -1) => {
            for y in position.1.. {
                if !map.contains_key(&(position.0, y - direction.1)) {
                    return ((position.0, y), direction);
                }
            }
        }
        _ => (),
    }
    (position, direction)
}

fn wrap_cube(position: (i32, i32)) -> ((i32, i32), (i32, i32)) {
    // test
    // let (next_position, next_direction) = match position {
    //     (8..=11, -1) => ((3 - position.0 % 4, 4), (0, 1)), // 1 - 2 (rev)
    //     (0..=3, 3) => ((11 - position.0 % 4, 0), (0, 1)),
    //
    //     (7, 0..=3) => ((position.1 % 4 + 4, 4), (0, 1)), // 1 - 3
    //     (4..=7, 3) => ((8, position.0 % 4), (1, 0)),
    //
    //     (12, 0..=3) => ((15, 11 - position.1 % 4), (-1, 0)), // 1 - 6 (rev)
    //     (16, 8..=11) => ((11, 3 - position.1 % 4), (-1, 0)),
    //
    //     (0..=3, 7) => ((11 - position.0 % 4, 11), (0, -1)), // 2 - 5 (rev)
    //     (8..=11, 11) => ((3 - position.0 % 4, 7), (0, -1)),
    //
    //     (0, 4..=7) => ((15 - position.1 % 4, 11), (0, -1)), // 2 - 6 (rev)
    //     (12..=15, 11) => ((0, 7 - position.0 % 4), (1, 0)),
    //
    //     (4..=7, 7) => ((8, 11 - position.0 % 4), (1, 0)), // 3 - 5 (rev)
    //     (8, 8..=11) => ((7 - position.1 % 4, 7), (0, -1)),
    //
    //     (11, 4..=7) => ((15 - position.1 % 4, 8), (0, 1)), // 4 - 6 (rev)
    //     (12..=15, 8) => ((11, 7 - position.0 % 4), (-1, 0)),
    //     _ => panic!("Missing wrap at {:?}", position),
    // };

    let (next_position, next_direction) = match position {
        (49, 0..=49) => ((0, 149 - position.1 % 50), (1, 0)), // println!("Left edge of 1 to left edge of 5 reversed"),
        (-1, 100..=149) => ((50, 49 - position.1 % 50), (1, 0)), // println!("Left edge of 5 to left edge of 1 reversed"),

        (50..=99, -1) => ((0, 150 + position.0 % 50), (1, 0)), // println!("Top edge of 1 to left edge of 6"),
        (-1, 150..=199) => ((50 + position.1 % 50, 0), (0, 1)), // println!("Left edge of 6 to top edge of 1"),

        (100..=149, -1) => ((position.0 % 50, 199), (0, -1)), //println!("Top edge of 2 to bottom edge of 6"),
        (0..=49, 200) => ((position.0 % 50 + 100, 0), (0, 1)), // println!("Bottom edge of 6 to top edge of 2"),

        (150, 0..=49) => ((99, 149 - position.1 % 50), (-1, 0)), //println!("Right edge of 2 to right edge of 4 reversed"),
        (100, 100..=149) => ((149, 49 - position.1 % 50), (-1, 0)), //println!("Right edge of 4 to right edge of 2 reversed"),

        (100..=149, 50) => ((99, position.0 % 50 + 50), (-1, 0)), //println!("Bottom edge of 2 to right edge of 3"),
        (100, 50..=99) => ((position.1 % 50 + 100, 49), (0, -1)), //println!("Right edge of 3 to bottom edge of 2"),

        (49, 50..=99) => ((position.1 % 50, 100), (0, 1)), //println!("Left edge of 3 to top edge of 5"),
        (0..=49, 99) => ((50, 50 + position.0 % 50), (1, 0)), //println!("Top edge of 5 to left edge of 3"),

        (50..=99, 150) => ((49, position.0 % 50 + 150), (-1, 0)), //println!("Bottom edge of 4 to right edge of 6"),
        (50, 150..=199) => ((50 + position.1 % 50, 149), (0, -1)), //println!("Right edge of 6 to bottom edge of 4"),
        _ => panic!("Missing wrap at {:?}", position),
    };

    (next_position, next_direction)
}

fn move_forward(
    amount: i32,
    mut position: (i32, i32),
    mut direction: (i32, i32),
    map: &HashMap<(i32, i32), char>,
    cube: bool,
) -> ((i32, i32), (i32, i32)) {
    for _ in 0..amount {
        let mut next_position = (position.0 + direction.0, position.1 + direction.1);
        let mut next_direction = direction;

        if map.get(&next_position).is_none() {
            (next_position, next_direction) = if !cube {
                wrap(position, direction, map)
            } else {
                wrap_cube(next_position)
            };
        }

        if *map.get(&next_position).unwrap() == '#' {
            return (position, direction);
        }

        position = next_position;
        direction = next_direction;
    }
    (position, direction)
}

fn rotate(direction: (i32, i32), left: bool) -> Option<(i32, i32)> {
    if direction == (1, 0) {
        if left {
            Some((0, -1))
        } else {
            Some((0, 1))
        }
    } else if direction == (-1, 0) {
        if left {
            Some((0, 1))
        } else {
            Some((0, -1))
        }
    } else if direction == (0, 1) {
        if left {
            Some((1, 0))
        } else {
            Some((-1, 0))
        }
    } else if direction == (0, -1) {
        if left {
            Some((-1, 0))
        } else {
            Some((1, 0))
        }
    } else {
        None
    }
}

fn travel_path(map: &HashMap<(i32, i32), char>, path: &Vec<Direction>, cube: bool) -> i32 {
    let mut position = (0, 0);
    let mut direction = (1, 0);

    for i in 1.. {
        if let Some(&v) = map.get(&(i, 0)) {
            if v == '.' {
                position = (i, 0);
                break;
            }
        }
    }

    for d in path {
        match d {
            Direction::Forward(f) => {
                (position, direction) = move_forward(*f, position, direction, map, cube)
            }
            Direction::RotateRight => direction = rotate(direction, false).unwrap(),
            Direction::RotateLeft => direction = rotate(direction, true).unwrap(),
        }
    }

    let facing = match direction {
        (1, 0) => 0,
        (0, 1) => 1,
        (-1, 0) => 2,
        (0, -1) => 3,
        _ => panic!("Wrong direction"),
    };

    1000 * (position.1 + 1) + 4 * (position.0 + 1) + facing
}

pub fn part1(input: &str) -> String {
    let (map, path) = parse(input);
    travel_path(&map, &path, false).to_string()
}

pub fn part2(input: &str) -> String {
    let (map, path) = parse(input);
    travel_path(&map, &path, true).to_string()
}
