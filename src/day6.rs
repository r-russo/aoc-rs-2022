use std::{cmp::Ordering, collections::VecDeque};

fn check_repeated(queue: &VecDeque<char>) -> bool {
    for (i, &c1) in queue.iter().enumerate() {
        for (j, &item) in queue.iter().enumerate().skip(i + 1) {
            if i == j {
                continue;
            }
            if c1 == item {
                return true;
            }
        }
    }
    false
}

fn get_unique_queue(input: &str, size: usize) -> i32 {
    let mut queue: VecDeque<char> = VecDeque::new();
    for (i, c) in input.chars().enumerate() {
        queue.push_front(c);

        match queue.len().cmp(&size) {
            Ordering::Less => continue,
            Ordering::Greater => queue.pop_back(),
            _ => None,
        };

        if !check_repeated(&queue) {
            return (i + 1) as i32;
        }
    }
    -1
}

pub fn part1(input: &str) -> String {
    get_unique_queue(input, 4).to_string()
}
pub fn part2(input: &str) -> String {
    get_unique_queue(input, 14).to_string()
}
